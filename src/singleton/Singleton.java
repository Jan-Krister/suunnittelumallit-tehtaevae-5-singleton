package singleton;

public class Singleton {

	private Singleton() {
	};

	private static Singleton INSTANCE = null;

	public static Singleton getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Singleton();
		}
		return INSTANCE;
	}	
	
	public Alpina luoAlpina() {
		return new Alpina();
	}
}
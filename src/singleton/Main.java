package singleton;

public class Main {

	public static void main(String[] args) {
		
		Singleton instance = Singleton.getInstance();
		Auto auto = instance.luoAlpina();
		System.out.println(auto.toString());

	}

}
